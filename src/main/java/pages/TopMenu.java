package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class TopMenu extends BasePage {

    @FindBy(xpath = "//ul[@class = 'menu-header']/li/a[contains(text(), 'Nas')]")
    private WebElement aboutUsButton;

    @FindBy(xpath = "//ul[@class = 'menu-header']/li/a[contains(text(), 'Newsy')]")
    private WebElement newsButton;

    @FindBy(xpath = "//ul[@class = 'menu-header']/li/a[contains(text(), 'Wyjazdy')]")
    private WebElement tripsButton;

    @FindBy(xpath = "//ul[@class = 'menu-header']/li/a[contains(text(), 'Galeria')]")
    private WebElement galleryButton;

    @FindBy(xpath = "//ul[@class = 'menu-header']/li/a[contains(text(), 'Treningi')]")
    private WebElement tutorialsButton;

    @FindBy(xpath = "//ul[@class = 'menu-header']/li//a[contains(text(), 'Logowanie')]")
    private WebElement loginButton;

    public TopMenu(WebDriver driver) {
        super(driver);
    }

    public AboutUsPage pressAboutUsButton() {
        aboutUsButton.click();
        return new AboutUsPage(driver);
    }

    public NewsPage pressNewsButtonButton() {
        newsButton.click();
        return new NewsPage(driver);
    }

    public TripsPage pressTripsButton() {
        tripsButton.click();
        return new TripsPage(driver);
    }

    public GalleryPage pressGalleryButton() {
        galleryButton.click();
        return new GalleryPage(driver);
    }

    public TutorialPage pressTutorialButton() {
        tutorialsButton.click();
        return new TutorialPage(driver);
    }

    public LoginPage pressLoginButton() {
        loginButton.click();
        return new LoginPage(driver);
    }

}
