package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.junit.Assert.assertEquals;

public class TripsPage extends TopMenu {

    @FindBy(xpath = "//h1[contains(text(), 'Wyjazdy')]")
    private WebElement pageTitle;
    public TripsPage(WebDriver driver) {
        super(driver);
    }

    public TripsPage assertPageTitle(String expectedVale) {
        assertEquals(expectedVale, pageTitle.getText().toLowerCase());
        return this;
    }

}
