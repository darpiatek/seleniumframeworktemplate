package pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.junit.Assert.assertEquals;

public class GalleryPage extends TopMenu {
    @FindBy(xpath = "//h1[contains(text(), 'Galeria')]")
    private WebElement pageTitle;

    public GalleryPage(WebDriver driver) {
        super(driver);
    }

    public GalleryPage assertPageTitle(String expectedVale) {
        assertEquals(expectedVale, pageTitle.getText().toLowerCase());
        return this;
    }
}
