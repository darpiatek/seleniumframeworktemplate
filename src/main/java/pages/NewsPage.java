package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.junit.Assert.assertEquals;

public class NewsPage extends TopMenu {

    @FindBy(xpath = "//h1[contains(text(), 'Newsy')]")
    private WebElement pageTitle;

    public NewsPage(WebDriver driver) {
        super(driver);
    }

    public NewsPage assertPageTitle(String expectedVale) {
        assertEquals(expectedVale, pageTitle.getText().toLowerCase());
        return this;
    }

}
