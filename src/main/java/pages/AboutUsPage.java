package pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.junit.Assert.assertEquals;

public class AboutUsPage extends TopMenu {
    @FindBy(xpath = "//div/span[contains(text(), 'Nas')]")
    private WebElement pageTitle;

    public AboutUsPage(WebDriver driver) {
        super(driver);
    }

    public AboutUsPage assertPageTitle(String expectedVale) {
        assertEquals(expectedVale, pageTitle.getText().toLowerCase());
        return this;
    }

}
