package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.junit.Assert.assertEquals;

public class TutorialPage extends TopMenu {

    @FindBy(xpath = "//h1[contains(text(), 'Treningi')]")
    private WebElement pageTitle;

    public TutorialPage(WebDriver driver) {
        super(driver);
    }

    public TutorialPage assertPageTitle(String expectedVale) {
        assertEquals(expectedVale, pageTitle.getText().toLowerCase());
        return this;
    }

}
