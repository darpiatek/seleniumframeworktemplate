import org.junit.jupiter.api.Tag;
import org.testng.annotations.Test;
import pages.HomePage;

public class ExampleTest extends BaseTest {

    @Test
    @Tag("Regression")
    public void goThroughPages() {
        new HomePage(driver)
                .pressAboutUsButton()
                .assertPageTitle("o nas")
                .pressNewsButtonButton()
                .assertPageTitle("newsy")
                .pressGalleryButton()
                .assertPageTitle("galeria")
                .pressTripsButton()
                .assertPageTitle("wyjazdy")
                .pressTutorialButton()
                .assertPageTitle("treningi")
                .pressLoginButton()
                .assertPageTitle("zaloguj się");
    }


    @Test
    @Tag("Regression")
    public void goThroughPagesAgain() {
        new HomePage(driver)
                .pressAboutUsButton()
                .assertPageTitle("o nas")
                .pressNewsButtonButton()
                .assertPageTitle("newsy")
                .pressGalleryButton()
                .assertPageTitle("galeria")
                .pressTripsButton()
                .assertPageTitle("wyjazdy")
                .pressTutorialButton()
                .assertPageTitle("treningi")
                .pressLoginButton()
                .assertPageTitle("zaloguj się");
    }

}
