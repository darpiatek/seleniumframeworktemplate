import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

import java.time.Duration;

public class BaseTest {
    protected static WebDriver driver;

    @BeforeMethod
    public void baseBeforeEach() {
        driver.get("https://yeti.org.pl/"); //replace page address with yours
    }

    @BeforeClass
    public static void baseBeforeClass() {
        ChromeOptions options = new ChromeOptions();
        // https://peter.sh/experiments/chromium-command-line-switches/
        options.addArguments("disable-infobars");
        options.addArguments("--disable-extensions");
        options.addArguments("--disable-gpu");
        options.addArguments("--disable-dev-shm-usage");
        options.addArguments("--no-sandbox");
        options.addArguments("--disable-in-process-stack-traces");
        options.addArguments("--disable-logging");
        options.addArguments("--log-level=3");
        options.addArguments("--remote-allow-origins=*");
        driver = new ChromeDriver(options);
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        driver.manage().window().maximize();
    }

//    @AfterMethod
//    public void baseAfterMethod() {
//        driver.manage().deleteAllCookies();
//    }

    @AfterClass
    public void baseAfterClass() {
        driver.quit();
    }
}
